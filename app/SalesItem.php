<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesItem extends Model
{
    protected $fillable = ['sale_id', 'product_name', 'product_length', 'product_width', 'material_length', 'material_width', 'material_id'];
    protected $primaryKey = 'sale_item_id';
}
