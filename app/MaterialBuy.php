<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialBuy extends Model
{
    protected $table = 'material_buys';
    protected $primaryKey = 'material_buy_id';
}
