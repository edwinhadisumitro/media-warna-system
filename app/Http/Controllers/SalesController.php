<?php

namespace App\Http\Controllers;

use App\Material;
use App\MaterialBuy;
use App\Sales;
use App\SalesItem;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    public function AllSales() {
        $sales = Sales::all();

        return view('sales.sale', ['sales'=>$sales]);
    }

    public function ShowSaveSales() {
        return view('sales.new');
    }

    public function SalesDetail($id) {
        $sale = Sales::find($id);
        $saleItem = SalesItem::where('sale_id', $id)->leftJoin('materials','materials.material_id','sales_items.material_id')->get();

        foreach ($saleItem as $i) {
            $material = Material::find($i->material_id);
            $i->material = $material;

            $supply = Material::find($i->supply_id);
            $i->supply = $supply;

            $product = Material::find($i->product_id);
            $i->product = $product;
        }

        $materials = Material::all();

        return view('sales.detail', ['sale' => $sale, 'sales_items' => $saleItem, 'materials' => $materials]);
    }

    public function SaveSales(Request $request) {
        $sales = new Sales();

        $sales->customer_name = $request->input('customer_name');

        $sales->save();

        return redirect('/sales/'.$sales->id);
    }

    public function AddSalesItem(Request $request, $id) {
        $materialTemp = Material::find($request->input('material_id'));
        $material = Material::where('name', $materialTemp->name)->where('width', $request->input('material_width'))->first();
        $product = Material::find($request->input('product_id'));

        $saleItem = new SalesItem();

        $saleItem->sale_id = $id;
        $saleItem->product_id = $request->input('product_id');
        $saleItem->product_length = $request->input('product_length');
        $saleItem->product_width = $request->input('product_width');
        $saleItem->material_length = 0;
        $saleItem->material_width = $material->width;
        $saleItem->supply_id = 0;
        $saleItem->supply_price = 0;
        $saleItem->material_id = $material->material_id;
        $saleItem->product_price = $product->price;
        $saleItem->material_price = $material->price;

        if ($request->input('supply_id') != 0) {
            $supply = Material::find($request->input('supply_id'));
            $saleItem->supply_id = $supply->material_id;
            $saleItem->supply_price = $supply->price;
        }

        $saleItem->save();

        return redirect('/sales/'.$id);
    }

    public function UpdateSales(Request $request, $id) {
        $sale = Sales::find($id);

        $sale->customer_name = $request->input('customer_name');

        $sale->save();

        return redirect('/sales/'.$id);
    }

    public function ShowInvoice($id) {
        $data = Sales::where('sales.sale_id', $id)->join('sales_items', 'sales.sale_id', 'sales_items.sale_id')->get();
        $grandTotal = 0;

        foreach ($data as $d) {
            $product = Material::find($d->product_id);
            $material = Material::find($d->material_id);

            $d->product = $product;
            $d->material = $material;

            $grandTotal += ($d->material_price * ceil($d->product_length) * ceil($d->product_width)) + $d->product_price + $d->supply_price;
        }

//        $pdf = PDF::loadView('invoice', ['data' => $data, 'grandTotal' => $grandTotal])->setPaper('a4');
//
//        return $pdf->download('invoice_'.$id.'.pdf');
        return view('invoice', ['data' => $data, 'grandTotal' => $grandTotal]);
    }

    public function DeleteSalesDetail($id) {
        $saleItem = SalesItem::find($id);
        $saleID = $saleItem->sale_id;

        $saleItem->delete();

        return redirect('/sales/'.$saleID);
    }
}
