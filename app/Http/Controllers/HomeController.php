<?php

namespace App\Http\Controllers;

use App\MaterialBuy;
use App\Sales;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function GetRevenue(Request $request) {
        $date = new Carbon();

        $month = $request['month'];

        $year = $date->year;

        if ($month == null) {
            $month = $date->month;
        }

        $sales = Sales::select(DB::raw('sales.sale_id as sale_id, sum((materials.price * sales_items.product_length * sales_items.product_width)+sales_items.product_price+sales_items.supply_price) as amount, sales.created_at as created_at'))->join('sales_items', 'sales.sale_id', 'sales_items.sale_id')->leftJoin('materials', 'materials.material_id', 'sales_items.material_id')->groupBy('sale_id', 'created_at')->where('sales.created_at', 'like', $year.'-'.$month.'%')->get();
        $capital = MaterialBuy::where('material_buys.created_at', 'like', $year.'-'.$month.'%')->leftJoin('materials', 'materials.material_id', 'material_buys.material_id')->select(['material_buys.created_at as spent_at', 'material_buys.price as price', 'materials.name as name', 'material_buys.qty as qty'])->get();

        $salesSum = Sales::select(DB::raw('sum((materials.price * sales_items.product_length * sales_items.product_width)+sales_items.product_price+sales_items.supply_price) as amount'))->join('sales_items', 'sales.sale_id', 'sales_items.sale_id')->leftJoin('materials', 'materials.material_id', 'sales_items.material_id')->where('sales.created_at', 'like', $year.'-'.$month.'%')->first();
        $capitalSum = MaterialBuy::where('material_buys.created_at', 'like', $year.'-'.$month.'%')->leftJoin('materials', 'materials.material_id', 'material_buys.material_id')->select(DB::raw('sum(material_buys.price * material_buys.qty) as qty'))->first();

        return view('revenue', ['sales' => $sales, 'capitals' => $capital, 'sales_sum' => $salesSum, 'capital_sum' => $capitalSum]);
    }

    public function SetRevenue(Request $request) {
        return redirect('/revenue?month='.$request->input('month'));
    }
}
