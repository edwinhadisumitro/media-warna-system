<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function showUser() {
        $users=DB::table('users')->paginate(20);

        return view('users.user', ['users'=>$users]);
    }

    public function showUserDetail($id) {
        $user=DB::table('users')->where('id',$id)->first();

        if(Auth::user()->role != $user->role && Auth::user()->role != 'Owner') {
            return redirect('/home');
        }

        return view('users.userdetail', ['user'=>$user]);
    }

    public function updateUser(Request $request, $id) {
        DB::table('users')->where('id', $id)->update([
            'email'=>$request->input('email'),
            'role'=>$request->input('role'),
            'updated_at'=>Carbon::now()->setTimezone('Asia/Jakarta')->toDateTimeString()
        ]);

        return redirect('/user/'.$id);
    }

    public function deleteUser($id) {
        DB::table('users')->where('id',$id)->delete();

        return redirect('/user');
    }

    public function changePassword($id) {
        return view('users.changepassword', ['id'=>$id]);
    }

    public function storePassword(Request $request) {
        $user = DB::table('users')->where('id',$request->input('user_id'))->first();

        if (Hash::check($request->input('old_password'), $user->password)) {
            if ($request->input('new_password') == $request->input('confirm_password')) {
                DB::table('users')->where('id',$request->input('user_id'))->update([
                    'password'=>Hash::make($request->input('new_password'))
                ]);
            }
        }

        return redirect('/user/'.$request->input('user_id'));
    }
}
