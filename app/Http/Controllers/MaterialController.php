<?php

namespace App\Http\Controllers;

use App\Material;
use App\MaterialBuy;
use App\Product;
use App\Supplier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MaterialController extends Controller
{
    public function AllMaterials() {
        $materials = DB::table('materials')->leftJoin('suppliers', 'suppliers.supplier_id', 'materials.supplier_id')->select(DB::raw('material_id, materials.name, width, length, qty, threshold, materials.name, if(qty <= threshold, "!!!", "-") as status, suppliers.name as supplier, materials.buy_price, materials.type'))->orderBy('material_id','asc')->get();

        return view('materials.material', ['materials' => $materials]);
    }

    public function AllProducts() {
        $materials = DB::table('materials')->leftJoin('suppliers', 'suppliers.supplier_id', 'materials.supplier_id')->select(DB::raw('material_id, materials.name, width, length, qty, threshold, materials.name, if(qty <= threshold, "!!!", "-") as status, suppliers.name as supplier, materials.buy_price, materials.type'))->where('type', 'Produk')->orderBy('material_id','asc')->get();

        return view('products.products', ['materials' => $materials]);
    }

    public function ShowNewProduct() {
        return view('products.new');
    }

    public function SaveMaterial(Request $request) {
        $material = new Material;

        $material->name = $request->input('name');
        $material->type = $request->input('type');
        $material->length = $request->input('length');
        $material->width = $request->input('width');
        $material->qty = $request->input('qty');
        $material->unit = 'Roll';
        $material->threshold = $request->input('threshold');
        $material->buy_price = $request->input('buy_price');
        $material->price = $request->input('price');
        $material->supplier_id = $request->input('supplier_id');
        $material->created_at = Carbon::now();
        $material->updated_at = Carbon::now();

        $material->save();

        return redirect('/materials');
    }

    public function DeleteMaterial($id) {
        $material = Material::find($id);

        $material->delete();

        return redirect('/materials');
    }

    public function ShowUpdateProduct($id) {
        $product = Material::find($id);

        return view('products.update', ['product' => $product]);
    }

    public function ShowBuyMaterial($id) {
        $material = Material::find($id);
        $suppliers = Supplier::all();

        return view('materials.buy', ['material' => $material, 'suppliers'=>$suppliers]);
    }

    public function ShowNewMaterial() {
        $suppliers = Supplier::all();

        return view('materials.new', ['suppliers' => $suppliers]);
    }

    public function SaveProduct(Request $request) {
        $product = new Material();

        $product->name = $request->input('name');
        $product->type = 'Produk';
        $product->qty = 0;
        $product->unit = 'Roll';
        $product->threshold = 0;
        $product->supplier_id = 0;
        $product->length = 0;
        $product->width = 0;
        $product->buy_price = 0;
        $product->price = 0;

        $product->save();

        return redirect('/products');
    }

    public function BuyMaterial(Request $request) {
        $material = Material::find($request->input('material_id'));

        $material->qty += $request->input('qty');
        $material->supplier_id = $request->input('supplier_id');

        $material->save();

        $materialBuy = new MaterialBuy();

        $materialBuy->material_id = $material->material_id;
        $materialBuy->price = $request->input('material_price');
        $materialBuy->qty = $request->input('qty');

        $materialBuy->save();

        return redirect('/materials');
    }

    public function UpdateProduct(Request $request, $id) {
        $product = Material::find($id);

        $product->name = $request->input('name');
        $product->type = 'Produk';
        $product->qty = 0;
        $product->unit = 'Roll';
        $product->threshold = 0;
        $product->supplier_id = 0;
        $product->length = $request->input('length');
        $product->width = $request->input('width');
        $product->buy_price = $request->input('buy_price');
        $product->price = $request->input('selling_price');

        $product->save();

        return redirect('/products');
    }

    public function GetAllMaterialByType($type) {
        $materials = Material::select(DB::raw('(select material_id from materials m where m.name=materials.name limit 1) as material_id, name'))->where('type',$type)->distinct()->get(['name']);

        return response($materials);
    }

    public function DeleteProduct($id) {
        $product = Material::find($id);

        $product->delete();

        return redirect('/products');
    }

    public function GetAllSameMaterialByName($id) {
        $material = Material::find($id);

        $materials = Material::where('name', $material->name)->get();

        return response($materials);
    }
}
