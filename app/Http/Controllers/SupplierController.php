<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function AllSuppliers() {
        $suppliers = Supplier::all();

        return view('suppliers.supplier', ['suppliers' => $suppliers]);
    }

    public function DeleteSupplier($id) {
        $supplier = Supplier::find($id);
        $supplier->delete();

        return redirect('/suppliers');
    }

    public function SupplierDetail($id) {
        $supplier = Supplier::find($id);

        return view('suppliers.detail', ['supplier' => $supplier]);
    }

    public function ShowSaveSupplier() {
        return view('suppliers.save');
    }

    public function SaveSupplier(Request $request) {
        $supplier = new Supplier();

        $supplier->name = $request->input('name');
        $supplier->contact_name = $request->input('contact_name');
        $supplier->contact_phone = $request->input('contact_phone');
        $supplier->address = $request->input('address');

        $supplier->save();

        return redirect('/suppliers');
    }

    public function UpdateSupplier(Request $request) {
        $supplier = Supplier::find($request->input('supplier_id'));

        $supplier->name = $request->input('name');
        $supplier->contact_name = $request->input('contact_name');
        $supplier->contact_phone = $request->input('contact_phone');
        $supplier->address = $request->input('address');

        $supplier->save();

        return redirect('/suppliers/'.$request->input('supplier_id'));
    }
}
