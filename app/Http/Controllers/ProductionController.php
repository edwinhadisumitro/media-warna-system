<?php

namespace App\Http\Controllers;

use App\Material;
use App\Sales;
use App\SalesItem;
use Illuminate\Http\Request;

class ProductionController extends Controller
{
    public function AllProduction() {
        $productions = Sales::all();

        return view('production.production', ['productions' => $productions]);
    }

    public function ProductionDetail($id) {
        $sale = Sales::find($id);
        $saleItem = SalesItem::where('sale_id', $id)->get();

        foreach ($saleItem as $i) {
            $material = Material::find($i->material_id);
            $i->material = $material;

            $supply = Material::find($i->supply_id);
            $i->supply = $supply;

            $product = Material::find($i->product_id);
            $i->product = $product;
        }

        return view('production.detail', ['sale' => $sale, 'sales_items' => $saleItem]);
    }

    public function UpdateProduction(Request $request) {
        $saleID = 0;

        foreach ($request->input('material_length') as $i => $length) {
            $saleItem = SalesItem::find($request->input('sale_item_id')[$i]);

            $saleID = $saleItem->sale_id;

            $material = Material::find($request->input('material_id')[$i]);

            $left = $material->length - $length;

            if ($left <= 0) {
                $material->qty--;
                $material->length = 80 + $left;
            } else {
                $material->length = $left;
            }

            $material->save();

            $saleItem->material_length = $length;

            $saleItem->save();
        }

        return redirect('/production/'.$saleID);
    }
}
