<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (Auth::user() != null) {
            if (Auth::user()->role == 'Owner') {
                if(Auth::user()->role != $role && $role != 'Admin' && $role != 'User'){
                    return redirect('/');
                }
            } else if (Auth::user()->role == 'Admin') {
                if(Auth::user()->role != $role && $role != 'User'){
                    return redirect('/');
                }
            } else {
                if(Auth::user()->role != $role){
                    return redirect('/');
                }
            }

        } else {
            return redirect('/');
        }


        return $next($request);
    }
}
