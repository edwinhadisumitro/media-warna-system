<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_items', function (Blueprint $table) {
            $table->increments('sale_item_id');
            $table->integer('sale_id');
            $table->integer('product_id');
            $table->float('product_length', 12, 0);
            $table->float('product_width', 12, 0);
            $table->float('material_length', 12, 0);
            $table->float('material_width', 12, 0);
            $table->integer('material_id');
            $table->integer('supply_id');
            $table->float('product_price', 12, 0);
            $table->float('material_price', 12, 0);
            $table->float('supply_price', 12, 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_items');
    }
}
