<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials', function (Blueprint $table) {
            $table->increments('material_id');
            $table->string('name');
            $table->string('type');
            $table->float('length', 12, 0);
            $table->float('width', 12, 0);
            $table->integer('qty');
            $table->string('unit');
            $table->integer('threshold');
            $table->float('buy_price', 12, 0);
            $table->float('price', 12, 0);
            $table->integer('supplier_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials');
    }
}
