@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ url('/suppliers/'.$supplier->supplier_id) }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="supplier_id" value="{{ $supplier->supplier_id }}">
            <div class="row">
                <div class="col-sm-8" style="float: none; margin: 0 auto">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Ubah Pemasok</strong></div>
                        <div class="panel-body">
                            <table class="table table-borderless">
                                <tr>
                                    <td>
                                        <label for="customer_address">Nama Pemasok</label>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control" value="{{ $supplier->name }}">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="customer_address">Nama Kontak</label>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input type="text" name="contact_name" class="form-control" value="{{ $supplier->contact_name }}">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="customer_address">Telepon Kontak</label>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input type="text" name="contact_phone" class="form-control" value="{{ $supplier->contact_phone }}">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="customer_address">Alamat Pemasok</label>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <textarea name="address" class="form-control">{{ $supplier->address }}</textarea>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <input type="submit" class="btn btn-primary" style="display: block; margin: 0 auto; width: 50%; margin-bottom: 50px;" value="Ubah">
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection