@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-1"><h3>Pemasok</h3></div>
        </div>

        <div class="row">
            <a href="{{ url('/suppliers/new') }}" class="btn btn-success" style="float: left; height: 25px; line-height: 10px; margin-bottom: 20px;">Tambah</a>
        </div>

        {{--<form action="{{ url('/customer') }}" method="get">--}}
        {{--<div class="row" style="margin-bottom: 20px;">--}}
        {{--<div class="col-sm-4">--}}
        {{--<input type="text" class="form-control" name="query">--}}
        {{--</div>--}}
        {{--<input type="submit" value="search" class="btn btn-default">--}}
        {{--</div>--}}
        {{--</form>--}}
        <table class="table table-bordered" id="orderTable">
            <tr style="cursor: default">
                <th>Kode Pemasok</th>
                <th>Nama Pemasok</th>
                <th>Kontak Pemasok</th>
                <th>Telepon Pemasok</th>
                <th>Alamat Pemasok</th>
                <th>Aksi</th>
            </tr>
            @foreach($suppliers as $supplier)
                <tr>
                    <td><a href="{{ url('/suppliers/'.$supplier->supplier_id) }}">{{ $supplier->supplier_id }}</a></td>
                    <td><a href="{{ url('/suppliers/'.$supplier->supplier_id) }}">{{ $supplier->name }}</a></td>
                    <td><a href="{{ url('/suppliers/'.$supplier->supplier_id) }}">{{ $supplier->contact_name }}</a></td>
                    <td><a href="{{ url('/suppliers/'.$supplier->supplier_id) }}">{{ $supplier->contact_phone }}</a></td>
                    <td><a href="{{ url('/suppliers/'.$supplier->supplier_id) }}">{{ $supplier->address }}</a></td>
                    <td><a href="{{ url('/suppliers/delete/'.$supplier->supplier_id) }}" class="btn btn-danger">Delete</a></td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection