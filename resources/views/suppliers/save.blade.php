@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ url('/suppliers') }}" method="post">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-8" style="float: none; margin: 0 auto">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Buat Pemasok</strong></div>
                        <div class="panel-body">
                            <table class="table table-borderless">
                                <tr>
                                    <td>
                                        <label for="customer_address">Nama Pemasok</label>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="customer_address">Nama Kontak</label>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input type="text" name="contact_name" class="form-control">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="customer_address">Telepon Kontak</label>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input type="text" name="contact_phone" class="form-control">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="customer_address">Alamat Pemasok</label>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <textarea name="address" class="form-control"></textarea>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <input type="submit" class="btn btn-primary" style="display: block; margin: 0 auto; width: 50%; margin-bottom: 50px;" value="Tambah">
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection