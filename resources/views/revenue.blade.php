@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h1>Pendapatan dan Pengeluaran</h1>
        </div>

        <form action="{{ url('/revenue/set') }}" method="post">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-1">
                    Bulan :
                </div>
                <div class="col-sm-2">
                    <select name="month" class="form-control" id="">
                        <option value="">Pilih bulan...</option>
                        <option value="01">Januari</option>
                        <option value="02">Februari</option>
                        <option value="03">Maret</option>
                        <option value="04">April</option>
                        <option value="05">Mei</option>
                        <option value="06">Juni</option>
                        <option value="07">Juli</option>
                        <option value="08">Agustus</option>
                        <option value="09">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <input type="submit" class="btn btn-primary" value="Pilih">
                </div>
            </div>
        </form>

        <div class="row">
            <div class="col-sm-6">
                <h2>Pendapatan</h2>
                <table class="table table-striped" id="orderTable">
                    <tr style="cursor: default">
                        <th>Tanggal Transaksi</th>
                        <th>Jumlah</th>
                    </tr>
                    @foreach($sales as $sale)
                       <tr>
                           <td><a href="{{ url('/sales/invoice/'.$sale->sale_id) }}" target="_blank">{{ $sale->created_at }}</a></td>
                           <td><a href="{{ url('/sales/invoice/'.$sale->sale_id) }}" target="_blank">Rp {{ $sale->amount }}</a></td>
                       </tr>
                    @endforeach
                    <tr>
                        <td><h3>Total</h3></td>
                        <td><h3>Rp {{ $sales_sum['amount'] }}</h3></td>
                    </tr>
                </table>
            </div>
            <div class="col-sm-6">
                <h2>Pengeluaran</h2>
                <table class="table table-striped" id="orderTable">
                    <tr style="cursor: default">
                        <th>Tanggal Beli</th>
                        <th>Nama</th>
                        <th>Jumlah</th>
                        <th>Jumlah</th>
                    </tr>
                    @foreach($capitals as $capital)
                        <tr>
                            <td>{{ $capital->spent_at}}</td>
                            <td>{{ $capital->name }}</td>
                            <td>{{ $capital->qty }}</td>
                            <td>Rp {{ $capital->price * $capital->qty }}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td><h3>Total</h3></td>
                        <td><h3>Rp {{ $capital_sum['qty'] }}</h3></td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
@endsection