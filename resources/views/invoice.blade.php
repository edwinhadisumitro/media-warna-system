<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Nota {{ $data[0]->sale_id }}</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        .col-print-1 {width:8%;  float:left;}
        .col-print-2 {width:16%; float:left;}
        .col-print-3 {width:25%; float:left;}
        .col-print-4 {width:33%; float:left;}
        .col-print-5 {width:42%; float:left;}
        .col-print-6 {width:50%; float:left;}
        .col-print-7 {width:58%; float:left;}
        .col-print-8 {width:66%; float:left;}
        .col-print-9 {width:75%; float:left;}
        .col-print-10{width:83%; float:left;}
        .col-print-11{width:92%; float:left;}
        .col-print-12{width:100%; float:left;}
    </style>
</head>
<body onload="print()">
<div class="container">
    <div class="row">
        <div class="col-print-6">
            <h1>Media Warna Scan</h1>
        </div>
        <div class="col-print-3 pull-right">
            <h1>Nota</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-print-3">
            <h4>Rincian Pembeli</h4>
            <table>
                <tr>
                    <td><strong>Nama pembeli : </strong></td>
                </tr>
                <tr>
                    <td>{{ $data[0]->customer_name }}</td>
                </tr>
            </table>
        </div>
        <div class="col-print-5 pull-right">
            <h4>Rincian Pesanan</h4>
            <table>
                <tr>
                    <td><strong>Kode Pesanan : </strong></td>
                </tr>
                <tr>
                    <td>{{ $data[0]->sale_id }}</td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row" style="margin-top: 20px;">
        <table class="table table-striped">
            <th>Produk</th>
            <th>Panjang Produk</th>
            <th>Lebar Produk</th>
            <th>Bahan</th>
            <th>Panjang Bahan</th>
            <th>Lebar Bahan</th>
            <th>Harga</th>
            @foreach($data as $d)
                <tr>
                    <td>{{ $d->product['name'] }}</td>
                    <td>{{ $d->product_length }} meter</td>
                    <td>{{ $d->product_width }} meter</td>
                    <td>{{ $d->material['name'] }}</td>
                    <td>{{ $d->material_length }} meter</td>
                    <td>{{ $d->material_width }} meter</td>
                    <td>Rp {{ number_format(($d->material_price * ceil($d->product_length) * ceil($d->product_width)) + $d->product_price + $d->supply_price) }}</td>
                </tr>
            @endforeach
        </table>
    </div>
    <div class="row">
        <div class="col-print-6 pull-right">
            <table class="table table-striped">
                <tr>
                    <td><h4>Grand Total</h4></td>
                    <td><h4>Rp {{ number_format($grandTotal) }}</h4></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<script>
    window.onload = function () {
        window.print()
    }
</script>
</body>
</html>