@extends('layouts.app')

@section('javascript')
    <script>
        const baseURL = 'http://media.edwinhadisumitro.io/'

        function NewXHR(method, endpoint, callback) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    callback(this.responseText)
                }
            };
            xhttp.open(method, endpoint, true);
            // xhttp.send();
            return xhttp
        }

        function MaterialBasedOnType(type) {
            let xhr = new NewXHR("GET", baseURL+"raw/material/"+type, (response) => {
                const products = JSON.parse(response)
                console.log(products)

                let inputForm = '<select name="material_id" required id="" class="form-control" onchange="MaterialWidth(this.value)"><option value="">Pilih bahan baku</option>'

                products.map(p => {
                    inputForm += '<option value="'+p.material_id+'">'+p.name+'</option>'
                })

                inputForm += '</select>'

                document.getElementById('material-input').innerHTML = inputForm
                document.getElementById('material-width-input').innerHTML = ''
                document.getElementById('material-length').innerHTML = ''
            })

            xhr.send()
        }

        function MaterialWidth(materialID) {
            let xhr = new NewXHR("GET", baseURL+"raw/material-same/"+materialID, (response) => {
                const products = JSON.parse(response)

                let inputForm = '<select name="material_width" required id="" class="form-control">'

                products.map(p => {
                    inputForm += '<option value="'+p.width+'">'+p.width+'</option>'
                })

                inputForm += '</select>'

                document.getElementById('material-width-input').innerHTML = inputForm
                document.getElementById('material-length').innerHTML = products[0].length
            })

            xhr.send()
        }
    </script>
@endsection

@section('content')
    <div class="container">
        <form action="{{ url('/sales/update/'.$sale->sale_id) }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="supplier_id" value="{{ $sale->sale_id }}">
            <div class="row">
                <div class="col-sm-8">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Penjualan #{{ $sale->sale_id }}</strong></div>
                        <div class="panel-body">
                            <table class="table table-borderless">
                                <tr>
                                    <td>
                                        <label for="customer_address">Nama Pembeli</label>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input type="text" name="customer_name" class="form-control" value="{{ $sale->customer_name }}">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <input type="submit" class="btn btn-primary" style="display: block; margin: 0 auto; width: 50%; margin-bottom: 50px;" value="Ubah">
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <form action="{{ url('/sales/'.$sale->sale_id) }}" method="post">
    {{ csrf_field() }}
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-striped">
                    <th>Nama Produk</th>
                    <th>Jenis</th>
                    <th>Panjang (meter)</th>
                    <th>Lebar (meter)</th>
                    <th>Bahan Baku</th>
                    <th>Panjang Bahan Baku</th>
                    <th>Lebar Bahan Baku</th>
                    {{--<th>Perlengkapan</th>--}}
                    <th>Subtotal</th>
                    <th>Aksi</th>
                    @foreach($sales_items as $si)
                        <tr>
                            <td>{{ $si->product['name'] }}</td>
                            <td>{{ $si->material['type'] }}</td>
                            <td>{{ $si->product_length }}</td>
                            <td>{{ $si->product_width }}</td>
                            <td>{{ $si->material['name'] }}</td>
                            <td>{{ $si->material['length'] }}</td>
                            <td>{{ $si->material_width }}</td>
{{--                            <td>@if($si->supply_id != 0) {{ $si->supply['name'] }} @endif</td>--}}
                            <td>{{ ($si->material_price * ceil($si->product_length) * ceil($si->product_width)) + $si->product_price + $si->supply_price }}</td>
                            <td><a href="{{ url('/sales/item/delete/'.$si->sale_item_id) }}" class="btn btn-danger">Hapus</a></td>
                        </tr>
                    @endforeach
                        <tr>
                            <td>
                                {{--<input type="text" required name="product_name" class="form-control">--}}
                                <select name="product_id" id="" class="form-control">
                                    <option value="">Pilih produk...</option>
                                    @foreach($materials as $material)
                                        @if ($material->type == 'Produk')
                                            <option value="{{ $material->material_id }}">{{ $material->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <select name="type" id="" class="form-control" onchange="MaterialBasedOnType(this.value)">
                                    <option value="">Pilih jenis...</option>
                                    <option value="Indoor">Indoor</option>
                                    <option value="Outdoor">Outdoor</option>
                                </select>
                            </td>
                            <td><input type="number" required name="product_length" step="any" class="form-control"></td>
                            <td><input type="number" required name="product_width" step="any" class="form-control"></td>
                            <td>
                                <div id="material-input">
                                    <select name="material_id" required id="" class="form-control" onchange="MaterialWidth(this.value)">
                                        <option value="0">Pilih bahan baku...</option>
                                        @foreach($materials as $material)
                                            @if ($material->type == 'Indoor' || $material->type == 'Outdoor')
                                                <option value="{{ $material->material_id }}">{{ $material->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </td>
                            <td>
                                <span id="material-length"></span>
                            </td>
                            <td>
                                <div id="material-width-input">

                                </div>
                            </td>
                            {{--<td>--}}
                                {{--<select name="supply_id" id="" class="form-control">--}}
                                    {{--<option value="">Pilih perlengkapan...</option>--}}
                                    {{--@foreach($materials as $material)--}}
                                        {{--@if($material->type == 'Perlengkapan')--}}
                                            {{--<option value="{{ $material->material_id }}">{{ $material->name }}</option>--}}
                                        {{--@endif--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                            {{--</td>--}}
                            <td></td>
                            <td><input type="submit" class="btn btn-primary" value="Tambahkan"></td>
                        </tr>
                </table>
            </div>
        </div>
        </form>

        <a href="{{ url('/sales/invoice/'.$sale->sale_id) }}" class="btn btn-default pull-right" target="_blank">Cetak Nota</a>
        {{--<input type="submit" class="btn btn-primary" style="display: block; margin: 0 auto; width: 50%; margin-bottom: 50px;" value="Edit">--}}
    </div>
@endsection