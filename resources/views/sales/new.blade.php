@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ url('/sales') }}" method="post">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-8">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Penjualan Baru</strong></div>
                        <div class="panel-body">
                            <table class="table table-borderless">
                                <tr>
                                    <td>
                                        <label for="customer_address">Nama Pembeli</label>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input type="text" name="customer_name" class="form-control">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <input type="submit" class="btn btn-primary" style="display: block; margin: 0 auto; width: 50%; margin-bottom: 50px;" value="Tambah">
        </form>
    </div>
@endsection