@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-1"><h3>Penjualan</h3></div>
        </div>

        <div class="row">
            <a href="{{ url('/sales/new') }}" class="btn btn-success" style="float: left; height: 25px; line-height: 10px; margin-bottom: 20px;">Tambahkan</a>
        </div>

        {{--<form action="{{ url('/customer') }}" method="get">--}}
        {{--<div class="row" style="margin-bottom: 20px;">--}}
        {{--<div class="col-sm-4">--}}
        {{--<input type="text" class="form-control" name="query">--}}
        {{--</div>--}}
        {{--<input type="submit" value="search" class="btn btn-default">--}}
        {{--</div>--}}
        {{--</form>--}}
        <table class="table table-bordered" id="orderTable">
            <tr style="cursor: default">
                <th>Kode Penjualan</th>
                <th>Nama Pembeli</th>
                <th>Tanggal Penjualan</th>
                <th>Tanggal Diperbaharui</th>
            </tr>
            @foreach($sales as $sale)
                <tr>
                    <td><a href="{{ url('/sales/'.$sale->sale_id) }}">{{ $sale->sale_id }}</a></td>
                    <td><a href="{{ url('/sales/'.$sale->sale_id) }}">{{ $sale->customer_name }}</a></td>
                    <td><a href="{{ url('/sales/'.$sale->sale_id) }}">{{ $sale->created_at }}</a></td>
                    <td><a href="{{ url('/sales/'.$sale->sale_id) }}">{{ $sale->updated_at }}</a></td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection