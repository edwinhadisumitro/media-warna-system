<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Media Warna Scan</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!--Icons-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Additional Javascript -->
    @yield('javascript')

</head>
<body>
<div id="app">
    <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <span class="navbar-brand" href="{{ url('/') }}">
                        @yield('page_name')
                    </span>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;<li><a href="{{ url('/') }}">Media Warna Scan</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @guest
                        <li><a href="{{ route('login') }}">Masuk</a></li>
                    @else
                        <li><a href="{{ url('/products') }}">Produk</a></li>
                        <li><a href="{{ url('/materials') }}">Pengelolaan Bahan Baku</a></li>
                        <li><a href="{{ url('/sales') }}">Penjualan</a></li>
                        <li><a href="{{ url('/production') }}">Produksi</a></li>
                        <li><a href="{{ url('/revenue') }}">Pendapatan & Pengeluaran</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                <span class="material-icons" style="margin: 0px; line-height: 15px; position: relative; top: 7px;">account_circle</span> {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                @if(\Illuminate\Support\Facades\Auth::user()->role == 'Owner')
                                    <li><a href="{{ url('/user') }}">Pengguna</a></li>
                                    <li><a href="{{ url('/suppliers') }}">Pemasok</a></li>
                                @elseif(\Illuminate\Support\Facades\Auth::user()->role == 'Admin')
                                    <li><a href="{{ url('/suppliers') }}">Pemasok</a></li>
                                @endif
                                <li><a href="{{ url('/user/'.\Illuminate\Support\Facades\Auth::id()) }}">Profil</a></li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <span class="material-icons" style="margin: 0px; line-height: 15px; position: relative; top: 7px;">exit_to_app</span> Keluar
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
