@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-1"><h3>Produk</h3></div>
        </div>

        <div class="row">
            <a href="{{ url('/products/new') }}" class="btn btn-success" style="float: left; height: 25px; line-height: 10px; margin-bottom: 20px;">Tambahkan</a>
        </div>

        {{--<form action="{{ url('/customer') }}" method="get">--}}
        {{--<div class="row" style="margin-bottom: 20px;">--}}
        {{--<div class="col-sm-4">--}}
        {{--<input type="text" class="form-control" name="query">--}}
        {{--</div>--}}
        {{--<input type="submit" value="search" class="btn btn-default">--}}
        {{--</div>--}}
        {{--</form>--}}
        <table class="table table-bordered" id="orderTable">
            <tr style="cursor: default">
                <th>Kode Produk</th>
                <th>Nama</th>
            </tr>
            @foreach($materials as $material)
                @if ($material->type == 'Produk')
                    <tr>
                        <td><a href="{{ url('/products/'.$material->material_id) }}">{{ $material->material_id }}</a></td>
                        <td><a href="{{ url('/products/'.$material->material_id) }}">{{ $material->name }}</a></td>
                    </tr>
                @endif
            @endforeach
        </table>
    </div>
@endsection