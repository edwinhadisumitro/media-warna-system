@extends('layouts.app')

@section('javascript')
    <script>
        function calculate() {
            const price = Number(document.getElementById('material-price').value)
            const qty = Number(document.getElementById('qty').value)

            document.getElementById('subtotal').innerHTML = 'Rp ' + price * qty
        }
    </script>
@endsection

@section('content')
    <div class="container">
        <form action="{{ url('/products/'.$product->material_id) }}" method="post">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-8" style="float: none; margin: 0 auto">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Produk</strong></div>
                        <div class="panel-body">
                            <table class="table table-borderless">
                                <tr>
                                    <td>
                                        <label for="customer_address">Nama Produk</label>
                                    </td>
                                    <td>
                                        <input type="text" name="name" value="{{ $product->name }}" class="form-control">
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <input type="submit" class="btn btn-primary" style="display: block; margin: 0 auto; width: 50%; margin-bottom: 50px;" value="Ubah">
                        <a href="{{ url('/products/delete/'.$product->material_id) }}" class="btn btn-danger" style="display: block; margin: 0 auto; width: 50%; margin-bottom: 50px;">Delete</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection