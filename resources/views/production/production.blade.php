@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-1"><h3>Produksi</h3></div>
        </div>

        {{--<div class="row">--}}
            {{--<a href="{{ url('/sales/new') }}" class="btn btn-success" style="float: left; height: 25px; line-height: 10px; margin-bottom: 20px;">Add new</a>--}}
        {{--</div>--}}

        {{--<form action="{{ url('/customer') }}" method="get">--}}
        {{--<div class="row" style="margin-bottom: 20px;">--}}
        {{--<div class="col-sm-4">--}}
        {{--<input type="text" class="form-control" name="query">--}}
        {{--</div>--}}
        {{--<input type="submit" value="search" class="btn btn-default">--}}
        {{--</div>--}}
        {{--</form>--}}
        <table class="table table-bordered" id="orderTable">
            <tr style="cursor: default">
                <th>Kode Penjualan</th>
                <th>Nama Pembeli</th>
                <th>Waktu Dibuat</th>
                <th>Waktu Dirubah</th>
            </tr>
            @foreach($productions as $production)
                <tr>
                    <td><a href="{{ url('/production/'.$production->sale_id) }}">{{ $production->sale_id }}</a></td>
                    <td><a href="{{ url('/production/'.$production->sale_id) }}">{{ $production->customer_name }}</a></td>
                    <td><a href="{{ url('/production/'.$production->sale_id) }}">{{ $production->created_at }}</a></td>
                    <td><a href="{{ url('/production/'.$production->sale_id) }}">{{ $production->updated_at }}</a></td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection