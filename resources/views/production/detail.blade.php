@extends('layouts.app')

@section('content')
    <div class="container">
        {{--<form action="{{ url('/sales/update/'.$sale->sale_id) }}" method="post">--}}
            {{--{{ csrf_field() }}--}}
            {{--<input type="hidden" name="supplier_id" value="{{ $sale->sale_id }}">--}}
            {{--<div class="row">--}}
                {{--<div class="col-sm-8">--}}
                    {{--<div class="panel panel-default">--}}
                        {{--<div class="panel-heading"><strong>Sale #{{ $sale->sale_id }}</strong></div>--}}
                        {{--<div class="panel-body">--}}
                            {{--<table class="table table-borderless">--}}
                                {{--<tr>--}}
                                    {{--<td>--}}
                                        {{--<label for="customer_address">Customer name</label>--}}
                                    {{--</td>--}}
                                    {{--<td>--}}
                                        {{--<div class="form-group">--}}
                                            {{--<input type="text" disabled name="customer_name" class="form-control" value="{{ $sale->customer_name }}">--}}
                                        {{--</div>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                            {{--</table>--}}
                            {{--<input type="submit" class="btn btn-primary" style="display: block; margin: 0 auto; width: 50%; margin-bottom: 50px;" value="Edit">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</form>--}}

        <form action="{{ url('/production/update') }}" method="post">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped">
                        <th>Nama Produk</th>
                        <th>Jenis</th>
                        <th>Panjang (meter)</th>
                        <th>Lebar (meter)</th>
                        <th>Bahan Baku</th>
                        <th>Ukuran Bahan Baku (meter)</th>
                        {{--<th>Perlengkapan</th>--}}
                        <th>Panjang Bahan Baku Terpakai</th>
                        {{--<th>Mata Ayam Terpakai</th>--}}
                        {{--<th>Aksi</th>--}}
                        @foreach($sales_items as $si)
                            {{--<tr>--}}
                                {{--<td>{{ $si->product->name }}</td>--}}
                                {{--<td>{{ $si->product_length }}</td>--}}
                                {{--<td>{{ $si->product_width }}</td>--}}
                                {{--<td>{{ $si->material->name }}</td>--}}
                                {{--<td>{{ $si->material_width }}</td>--}}
                                {{--<td><input type="number" name="material_length" @if($si->material_length > 0) disabled @endif  class="form-control" value="{{ $si->material_length }}"></td>--}}
                                {{--<td><input type="number" name="mata_ayam" @if($si->material_length > 0) disabled @endif class="form-control"></td>--}}
                                {{--<input type="hidden" name="sale_item_id" value="{{ $si->sale_item_id }}">--}}
                                {{--<input type="hidden" name="material_id" value="{{ $si->material_id }}">--}}
                                {{--<td> @if($si->material_length == 0) <input type="submit" class="btn btn-primary" value="Update"> @endif</td>--}}
                            {{--</tr>--}}
                            <tr>
                                <td>{{ $si->product['name'] }}</td>
                                <td>{{ $si->material['type'] }}</td>
                                <td>{{ $si->product_length }}</td>
                                <td>{{ $si->product_width }}</td>
                                <td>{{ $si->material['name'] }}</td>
                                <td>{{ $si->material['width'].' x '.$si->material['length'] }}</td>
                                {{--<td>@if($si->supply_id != 0) {{ $si->supply['name'] }} @endif</td>--}}
                                <td>@if($si->material_length > 0) <input type="number" step="any" name="material_length[]" disabled class="form-control" value="{{ $si->material_length }}"> @elseif($si->material_length <= 0) <input type="number" step="any" name="material_length[]" class="form-control" value="{{ $si->material_length }}"> @endif</td>
{{--                                <td><input type="number" name="mata_ayam[]" @if($si->material_length > 0) disabled @endif class="form-control"></td>--}}
                                @if($si->material_length <= 0) <input type="hidden" name="sale_item_id[]" value="{{ $si->sale_item_id }}"> @endif
                                @if($si->material_length <= 0) <input type="hidden" name="material_id[]" value="{{ $si->material_id }}"> @endif
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>

            <input type="submit" class="btn btn-primary" style="display: block; margin: 0 auto; width: 50%; margin-bottom: 50px;" value="Perbaharui">
        </form>


        {{--<input type="submit" class="btn btn-primary" style="display: block; margin: 0 auto; width: 50%; margin-bottom: 50px;" value="Edit">--}}
    </div>
@endsection