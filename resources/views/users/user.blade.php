@extends('layouts.app')

@section('page_name')
    <img src="{{ asset('img/logo_white.png') }}" alt="" height="30px;">
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-1"><h3>Pengguna</h3></div>
        </div>
        <a href="{{ url('/register') }}" class="btn btn-success" style="float: left; height: 25px; line-height: 10px; margin-bottom: 20px;">Tambah</a>
        <table class="table table-bordered" id="orderTable">
            <tr style="cursor: default">
                <th onclick="sortTable(3)">Kode Pengguna</th>
                <th onclick="sortTable(1)">Nama</th>
                <th onclick="sortTable(2)">Nama Pengguna</th>
                <th onclick="sortTable(3)">Jabatan</th>
                <th>Waktu dibuat</th>
                <th>Aksi</th>
            </tr>
            @foreach($users as $user)
                @if($user->id != 99)
                    <tr>
                        <td><a href="{{ url('/user/'.$user->id) }}">{{ $user->id }}</a></td>
                        <td><a href="{{ url('/user/'.$user->id) }}">{{ $user->name }}</a></td>
                        <td><a href="{{ url('/user/'.$user->id) }}">{{ $user->email }}</a></td>
                        <td><a href="{{ url('/user/'.$user->id) }}">{{ $user->role }}</a></td>
                        <td><a href="{{ url('/user/'.$user->id) }}">{{ date_format(date_create($user->created_at), 'l, d F Y') }}</a></td>
                        <td><a href="{{ url('/user/delete/'.$user->id) }}" class="btn btn-danger">Hapus</a></td>
                    </tr>
                @endif
            @endforeach
        </table>
    </div>
@endsection