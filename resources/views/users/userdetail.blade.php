@extends('layouts.app')

@section('page_name')
    <img src="{{ asset('img/logo_white.png') }}" alt="" height="30px;">
@endsection

@section('content')
    <div class="container">
        <form action="{{ url('/user/update/'.$user->id) }}" method="post">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Informasi Pengguna</strong></div>
                        <div class="panel-body">
                            <h2>{{ $user->name }}</h2>
                            <p>Nama pengguna : <input type="text" name="email" class="form-control" value="{{ $user->email }}"></p>
                            <p>
                                Jabatan :
                                <select name="role" class="form-control">
                                    <option value="Owner" @if($user->role=='Owner') selected @endif>Pemilik</option>
                                    <option value="Admin" @if($user->role=='Admin') selected @endif>Admin</option>
                                    <option value="User" @if($user->role=='User') selected @endif>Pengguna</option>
                                </select>
                            </p>
                            <p>Created at : <strong>{{ date_format(date_create($user->created_at), 'l, d F Y') }}</strong></p>
                            <p>Updated by : <strong>{{ date_format(date_create($user->updated_at), 'l, d F Y') }}</strong></p>
                            <a href="{{ url('/user/change-password/'.$user->id) }}" class="btn btn-danger">Ubah kata sandi</a> <input
                                    type="submit" value="Update" class="btn btn-success">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection