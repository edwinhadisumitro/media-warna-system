@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ url('/user/change-password') }}" method="post">
            <div class="row">
                <div class="col-sm-6" style="float: none; margin: 0 auto;">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Ganti Kata Sandi</strong></div>
                        <div class="panel-body">
                            <table class="table table-hover">
                                {{ csrf_field() }}
                                <input type="hidden" name="user_id" value="{{ $id }}">
                                <div class="form-group">
                                    <label for="method">Kata Sandi Lama</label>
                                    <input type="password" class="form-control" name="old_password">
                                </div>
                                <div class="form-group">
                                    <label for="amount">Kata Sandi Baru</label>
                                    <input type="password" class="form-control" name="new_password">
                                </div>
                                <div class="form-group">
                                    <label for="amount">Verifikasi Kata Sandi Baru</label>
                                    <input type="password" class="form-control" name="confirm_password">
                                </div>
                                <input type="submit" value="Change Password" class="btn btn-success">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection