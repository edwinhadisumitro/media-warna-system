@extends('layouts.app')

@section('javascript')
    <script>
        function calculate() {
            const price = Number(document.getElementById('material-price').value)
            const qty = Number(document.getElementById('qty').value)

            document.getElementById('subtotal').innerHTML = 'Rp ' + price * qty
        }
    </script>
@endsection

@section('content')
    <div class="container">
        <form action="{{ url('/materials/new') }}" method="post">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-8" style="float: none; margin: 0 auto">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Bahan Baku Baru</strong></div>
                        <div class="panel-body">
                            <table class="table table-borderless">
                                <tr>
                                    <td>
                                        <label for="customer_address">Nama Bahan Baku</label>
                                    </td>
                                    <td>
                                        <input type="text" name="name" required class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="customer_address">Tipe Bahan Baku</label>
                                    </td>
                                    <td>
                                        <select name="type" class="form-control" required>
                                            <option value="">Pilih tipe bahan baku...</option>
                                            <option value="Indoor">Indoor</option>
                                            <option value="Outdoor">Outdoor</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="customer_address">Panjang</label>
                                    </td>
                                    <td>
                                        <input type="number" step="any" name="length" required class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="customer_address">Lebar</label>
                                    </td>
                                    <td>
                                        <input type="number" step="any" name="width" required class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="customer_address">Jumlah Bahan Baku</label>
                                    </td>
                                    <td>
                                        <input type="text" name="qty" required class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="customer_address">Batas Bawah Bahan Baku</label>
                                    </td>
                                    <td>
                                        <input type="text" name="threshold" required class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="customer_address">Harga beli</label>
                                    </td>
                                    <td>
                                        <input type="number" step="any" name="buy_price" required class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="customer_address">Harga jual</label>
                                    </td>
                                    <td>
                                        <input type="number" step="any" name="price" required class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="customer_address">Pemasok</label>
                                    </td>
                                    <td>
                                        <select name="supplier_id" class="form-control" required>
                                            <option value="">Pilih pemasok...</option>
                                            @foreach($suppliers as $supplier)
                                                <option value="{{ $supplier->supplier_id }}">{{ $supplier->name }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <input type="submit" class="btn btn-primary" style="display: block; margin: 0 auto; width: 50%; margin-bottom: 50px;" value="Tambah">
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection