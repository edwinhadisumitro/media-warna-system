@extends('layouts.app')

@section('javascript')
    <script>
        function calculate() {
            const price = Number(document.getElementById('material-price').value)
            const qty = Number(document.getElementById('qty').value)

            document.getElementById('subtotal').innerHTML = 'Rp ' + price * qty
        }
    </script>
@endsection

@section('content')
    <div class="container">
        <form action="{{ url('/materials') }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="material_id" value="{{ $material->material_id }}">
            <div class="row">
                <div class="col-sm-8" style="float: none; margin: 0 auto">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong>Pembelian Bahan Baku</strong>
                        </div>
                        <div class="panel-body">
                            <table class="table table-borderless">
                                <tr>
                                    <td>
                                        <label for="customer_address">Nama Bahan Baku</label>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <input type="text" name="name" disabled class="form-control" value="{{ $material->name }}">
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="number" class="form-control" value="{{ $material->length }}" disabled>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="number" class="form-control" value="{{ $material->width }}" disabled>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="customer_address">Harga Satuan</label>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-1">
                                                    Rp
                                                </div>
                                                <div class="col-sm-11">
                                                    <input type="number" id="material-price" onchange="calculate()" name="material_price" value="{{ $material->buy_price }}" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="customer_address">Jumlah</label>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input required type="number" id="qty" onchange="calculate()" name="qty" class="form-control">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="customer_address">Harga total</label>
                                    </td>
                                    <td>
                                      <span id="subtotal"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="customer_address">Pemasok</label>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select name="supplier_id" required id="" class="form-control">
                                                <option value="">Pilih pemasok...</option>
                                                @foreach($suppliers as $supplier)
                                                    <option value="{{ $supplier->supplier_id }}">{{ $supplier->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <input type="submit" class="btn btn-primary" style="display: block; margin: 0 auto; width: 50%; margin-bottom: 50px;" value="Tambah">
                        <a href="{{ url('/material/delete/'.$material->material_id) }}" style="display: block; margin: 0 auto; width: 50%; margin-bottom: 50px;" class="btn btn-danger">Hapus</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection