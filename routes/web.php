<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

//Auth::routes();
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// Application Routes
Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/user', 'UserController@AllUsers')->middleware('role:Owner');
//Route::get('/user/{id}', 'UserController@UserDetail');
//Route::post('/user', 'UserController@SaveUser')->middleware('role:Owner');
//Route::put('/user/{id}', 'UserController@UpdateUser')->middleware('role:Owner');

Route::get('/user', 'UserController@showUser')->middleware('role:Owner');
Route::get('/user/new', 'UserController@showUserForm')->middleware('role:Owner');
Route::get('/user/{id}', 'UserController@showUserDetail');
Route::get('/user/delete/{id}', 'UserController@deleteUser')->middleware('role:Owner');

Route::get('/user/change-password/{id}','UserController@changePassword');
Route::post('/user/change-password','UserController@storePassword');

Route::post('/user/update/{id}', 'UserController@updateUser')->middleware('role:Owner');

Route::get('/revenue','HomeController@GetRevenue')->middleware('role:Owner');
Route::get('/revenue/{month}','HomeController@GetRevenueByMonth')->middleware('role:Owner');
Route::post('/revenue/set','HomeController@SetRevenue')->middleware('role:Owner');

Route::get('/suppliers', 'SupplierController@AllSuppliers')->middleware('role:Admin');
Route::get('/suppliers/new', 'SupplierController@ShowSaveSupplier')->middleware('role:Admin');
Route::get('/suppliers/{id}', 'SupplierController@SupplierDetail')->middleware('role:Admin');
Route::get('/suppliers/delete/{id}', 'SupplierController@DeleteSupplier')->middleware('role:Admin');
Route::post('/suppliers', 'SupplierController@SaveSupplier')->middleware('role:Admin');
Route::post('/suppliers/{id}', 'SupplierController@UpdateSupplier')->middleware('role:Admin');

Route::get('/materials', 'MaterialController@AllMaterials')->middleware('role:Admin');
Route::get('/materials/new', 'MaterialController@ShowNewMaterial')->middleware('role:Admin');
Route::get('/materials/{id}', 'MaterialController@ShowBuyMaterial')->middleware('role:Admin');
Route::post('/materials', 'MaterialController@BuyMaterial')->middleware('role:Admin');
Route::post('/materials/new', 'MaterialController@SaveMaterial')->middleware('role:Admin');
Route::get('/material/delete/{id}', 'MaterialController@DeleteMaterial')->middleware('role:Admin');
Route::get('/raw/material/{type}', 'MaterialController@GetAllMaterialByType');
Route::get('/raw/material-same/{id}', 'MaterialController@GetAllSameMaterialByName');

Route::get('/products', 'MaterialController@AllProducts')->middleware('role:Admin');
Route::get('/products/new', 'MaterialController@ShowNewProduct')->middleware('role:Admin');
Route::get('/products/{id}', 'MaterialController@ShowUpdateProduct')->middleware('role:Admin');
Route::post('/products/{id}', 'MaterialController@UpdateProduct')->middleware('role:Admin');
Route::post('/products', 'MaterialController@SaveProduct')->middleware('role:Admin');
Route::get('/products/delete/{id}', 'MaterialController@DeleteProduct')->middleware('role:Admin');

Route::get('/sales', 'SalesController@AllSales')->middleware('role:Admin');
Route::get('/sales/new', 'SalesController@ShowSaveSales')->middleware('role:Admin');
Route::get('/sales/invoice/{id}', 'SalesController@ShowInvoice')->middleware('role:Admin');
Route::get('/sales/{id}', 'SalesController@SalesDetail')->middleware('role:Admin');
Route::get('/sales/item/delete/{id}', 'SalesController@DeleteSalesDetail')->middleware('role:Admin');
Route::post('/sales', 'SalesController@SaveSales')->middleware('role:Admin');
Route::post('/sales/{id}', 'SalesController@AddSalesItem')->middleware('role:Admin');
Route::post('/sales/update/{id}', 'SalesController@UpdateSales')->middleware('role:Admin');

Route::get('/production', 'ProductionController@AllProduction')->middleware('role:User');
Route::get('/production/{id}', 'ProductionController@ProductionDetail')->middleware('role:User');
Route::post('/production/update', 'ProductionController@UpdateProduction')->middleware('role:User');
